func! smart#delete_word() abort
    let l:iskeyword = &iskeyword

    try
        set iskeyword&vim
        set iskeyword-=_

        if col('.') == col('$') - 1
            exec "norm! a\<C-w>\<right>"
        else
            exec "norm! i\<C-w>\<right>"
        end
    finally
        let &iskeyword = l:iskeyword
    endtry
endfunc

func! smart#fuzzy_search() abort
    if &wildcharm == 0 | echoerr 'You need to set wildcharm to some value' | endif
    if wildmenumode() | return nr2char(&wildcharm) | endif

    if getcmdtype() =~? '[/?]'
        let l:cmdline = getcmdline()
        let l:dot = l:cmdline =~# '^\\V' || !&magic ? '\.' : '.'
        let l:any = l:cmdline =~# '^\\v' ? '{-}' : '\{-}'

        return l:dot.l:any
    endif

    return nr2char(&wildcharm)
endfunc
