# Smart Vim

Vim is good boye, but this makes it also smart boye.

## Installation

Check your own favourite plugin manager docs or `:h packadd` if you do not use
any.

## Features

- Smart `<Tab>` when searching
- Smart `<C-w>` in insert mode

## License

See [LICENSE](LICENSE) file.
